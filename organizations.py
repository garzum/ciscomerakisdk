from meraki_sdk.meraki_sdk_client import MerakiSdkClient

API_KEY = '6bec40cf957de430a6f1f2baa056b99a4fac9ea0'

meraki = MerakiSdkClient(API_KEY)

organizations = meraki.organizations.get_organizations()

for organization in organizations:
    print('ID: {id}, Name: {name}'.format(id=organization['id'], name=organization['name']))