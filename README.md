# CiscoMerakiSDK

Three scripts presenting usage of Cisco Meraki SDK.

## Requirements

To run this script, you need to have Meraki SDK library. You can install it by using pip:
```
$ pip install meraki-sdk
```

## Usage

Documentation and examples can be found on my blog - https://garzum.net/cisco-meraki-rest-api-calls-using-sdk