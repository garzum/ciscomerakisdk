from meraki_sdk.meraki_sdk_client import MerakiSdkClient

API_KEY = '6bec40cf957de430a6f1f2baa056b99a4fac9ea0'

meraki = MerakiSdkClient(API_KEY)

organizations = meraki.organizations.get_organizations()

for organization in organizations:
    print('ID: {id}, Name: {name}'.format(id=organization['id'], name=organization['name']))

    collect = {
        'organization_id': organization['id'],
    }

    networks = meraki.networks.get_organization_networks(collect)

    if len(networks) > 0:
        print('Printing networks for organization {name}:'.format(name=organization['name']))

        for network in networks:
            print('\tID: {id}, Name: {name}, Type: {type}'.format(id=network['id'],
                                                                  name=network['name'],
                                                                  type=network['type']))

        devices = meraki.devices.get_network_devices(network['id'])

        for device in devices:
                if 'name' in device:
                    print('\tName: {name}, Model: {model}, Serial: {serial}'.format(name=device['name'],
                                                                                    model=device['model'],
                                                                                    serial=device['serial']))
                else:
                    print('\tModel: {model}, Serial: {serial}'.format(model=device['model'],
                                                                      serial=device['serial']))
        else:
            print('There are no devices assigned to the network {network}'.format(network=network['name']))